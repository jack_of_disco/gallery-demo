package com.zyl.callery_java.pojo;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Pixabay {
    private int total;
    private int totalHits;
    private List<PhotoItem> hits;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getTotalHits() {
        return totalHits;
    }

    public void setTotalHits(int totalHits) {
        this.totalHits = totalHits;
    }

    public List<PhotoItem> getHits() {
        return hits;
    }

    public void setHits(List<PhotoItem> hits) {
        this.hits = hits;
    }

    public class PhotoItem {
        @SerializedName("id")
        private int photoId;
        @SerializedName("webformatURL")
        private String previewUrl;
        @SerializedName("largeImageURL")
        private String fullUrl;

        public int getPhotoId() {
            return photoId;
        }

        public void setPhotoId(int photoId) {
            this.photoId = photoId;
        }

        public String getPreviewUrl() {
            return previewUrl;
        }

        public void setPreviewUrl(String previewUrl) {
            this.previewUrl = previewUrl;
        }

        public String getFullUrl() {
            return fullUrl;
        }

        public void setFullUrl(String fullUrl) {
            this.fullUrl = fullUrl;
        }
    }
}


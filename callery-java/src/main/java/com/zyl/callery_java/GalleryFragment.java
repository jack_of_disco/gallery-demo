package com.zyl.callery_java;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.os.Handler;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.zyl.callery_java.adapter.GalleryAdapter;
import com.zyl.callery_java.pojo.Pixabay;
import com.zyl.callery_java.viewmodel.GalleryViewModel;

import java.util.List;


public class GalleryFragment extends Fragment {

    private GalleryViewModel galleryViewModel;
    private SwipeRefreshLayout swipeRefreshLayout;
    private RecyclerView recyclerView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_gallery, container, false);
        swipeRefreshLayout = view.findViewById(R.id.swipeRefreshLayout);
        recyclerView = view.findViewById(R.id.recyclerView);
        return view;
    }

    public GalleryFragment() {
        setHasOptionsMenu(true);//显示menu
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu,menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.refresh:
                swipeRefreshLayout.setRefreshing(true); //改为刷新状态
                //动画持续一秒
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        galleryViewModel.fetchData();
                    }
                },1000);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        final GalleryAdapter galleryAdapter = new GalleryAdapter();

        recyclerView.setAdapter(galleryAdapter);
        recyclerView.setLayoutManager(new GridLayoutManager(requireActivity(), 2));

        galleryViewModel = new ViewModelProvider(this, new ViewModelProvider.AndroidViewModelFactory(requireActivity().getApplication())).get(GalleryViewModel.class);

        galleryViewModel.getLiveData().observe(this, new Observer<List<Pixabay.PhotoItem>>() {
            @Override
            public void onChanged(List<Pixabay.PhotoItem> photoItems) {
                galleryAdapter.submitList(photoItems);
                swipeRefreshLayout.setRefreshing(false);
            }
        });

        //初次进入界面自动刷新
        if(galleryViewModel.getLiveData().getValue()==null){
            galleryViewModel.fetchData();
        }

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                galleryViewModel.fetchData();
            }
        });

    }
}
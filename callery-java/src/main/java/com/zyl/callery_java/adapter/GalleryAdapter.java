package com.zyl.callery_java.adapter;


import android.graphics.drawable.Drawable;
import android.media.Image;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.NavigationUI;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.zyl.callery_java.R;
import com.zyl.callery_java.pojo.Pixabay;

import io.supercharge.shimmerlayout.ShimmerLayout;
import uk.co.senab.photoview.PhotoView;

public class GalleryAdapter extends ListAdapter<Pixabay.PhotoItem, GalleryAdapter.MyHolder> {

    public GalleryAdapter() {
        super(new DiffUtil.ItemCallback<Pixabay.PhotoItem>() {
            @Override
            public boolean areItemsTheSame(@NonNull Pixabay.PhotoItem oldItem, @NonNull Pixabay.PhotoItem newItem) {
                return oldItem.equals(newItem);
            }

            @Override
            public boolean areContentsTheSame(@NonNull Pixabay.PhotoItem oldItem, @NonNull Pixabay.PhotoItem newItem) {
                return oldItem.getPhotoId() == newItem.getPhotoId();
            }
        });
    }

    @NonNull
    @Override
    public MyHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View inflate = LayoutInflater.from(parent.getContext()).inflate(R.layout.gallery_cell, parent, false);
        final MyHolder myHolder = new MyHolder(inflate);
        myHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString("PHOTO", getItem(myHolder.getAdapterPosition()).getFullUrl());
                NavController controller = Navigation.findNavController(v);
                controller.navigate(R.id.action_galleryFragment_to_photoFragment, bundle);
            }
        });

        return myHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final MyHolder holder, int position) {
        holder.shimmerLayoutCell.setShimmerAngle(0);
        holder.shimmerLayoutCell.setShimmerColor(0x55FFFFFF);
        holder.shimmerLayoutCell.startShimmerAnimation();
        Glide.with(holder.itemView)
                .load(getItem(position).getPreviewUrl())
                .placeholder(R.drawable.ic_baseline_photo_24)
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        if (holder.shimmerLayoutCell != null) {
                            holder.shimmerLayoutCell.stopShimmerAnimation();
                        }
                        return false;
                    }
                })
                .into(holder.photoView);
    }


    class MyHolder extends RecyclerView.ViewHolder {
        ImageView photoView;
        ShimmerLayout shimmerLayoutCell;

        public MyHolder(@NonNull View itemView) {
            super(itemView);
            photoView = itemView.findViewById(R.id.imageViewCell);
            shimmerLayoutCell = itemView.findViewById(R.id.shimmerLayoutCell);
        }
    }
}

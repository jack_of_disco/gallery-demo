package com.zyl.callery_java.viewmodel;

import android.app.Application;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.zyl.callery_java.pojo.Pixabay;
import com.zyl.callery_java.volley.VolleySingleton;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class GalleryViewModel extends AndroidViewModel {
    private MutableLiveData<List<Pixabay.PhotoItem>> mutableLiveData = new MutableLiveData<>();


    public LiveData<List<Pixabay.PhotoItem>> getLiveData() {
        return mutableLiveData;
    }

    public GalleryViewModel(@NonNull Application application) {
        super(application);
    }

    public void fetchData() {
        StringRequest stringRequest = new StringRequest(
                Request.Method.GET,
                getUrl(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Gson gson = new Gson();
                        Pixabay pixabay = gson.fromJson(response, Pixabay.class);
                        mutableLiveData.setValue(pixabay.getHits());
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("myTAG", "onErrorResponse: ", error);
                    }
                }
        );

        VolleySingleton.getInstance(getApplication()).getRequestQueue().add(stringRequest);
    }

    public String getUrl() {
        Random random = new Random();
        return "https://pixabay.com/api/?key=20367884-4ff0b66c3d8b80b87f646de62&q=" + keyWord.get(random.nextInt(keyWord.size()));
    }

    private List<String> keyWord = Arrays.asList("car", "big", "word", "net", "photo");

}

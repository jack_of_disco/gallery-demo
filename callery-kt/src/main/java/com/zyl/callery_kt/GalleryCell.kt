package com.zyl.callery_kt

import android.graphics.drawable.Drawable
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import kotlinx.android.synthetic.main.gallery_cell.view.*

class GalleryCell : ListAdapter<MyPhoto, MyViewHolder>(DIFFCALLBACK) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val holder = MyViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.gallery_cell, parent, false))
        holder.itemView.setOnClickListener {
            Bundle().apply {
                putParcelable("PHOTO", getItem(holder.adapterPosition))//adapterPosition
                holder.itemView.findNavController().navigate(R.id.action_galleryFragment_to_photoFragment,this)
            }
        }
        return holder;
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.itemView.shimmerLayoutCell.apply {
            setShimmerAngle(0) //闪烁的角度
            setShimmerColor(0x55FFFFFF) //闪烁的颜色
            startShimmerAnimation()  //开始闪烁
        }
        Glide.with(holder.itemView)
                .load(getItem(position).previewUrl)
                .placeholder(R.drawable.ic_photo_gray_24dp)
                .listener(object : RequestListener<Drawable> {
                    override fun onLoadFailed(
                            e: GlideException?,
                            model: Any?,
                            target: Target<Drawable>?,
                            isFirstResource: Boolean
                    ): Boolean {
                        return false
                    }

                    override fun onResourceReady(
                            resource: Drawable?,
                            model: Any?,
                            target: Target<Drawable>?,
                            dataSource: DataSource?,
                            isFirstResource: Boolean
                    ): Boolean {
                        return false.also { holder.itemView.shimmerLayoutCell?.stopShimmerAnimation() }
                    }

                })
                .into(holder.itemView.imageViewCell)
    }


    object DIFFCALLBACK : DiffUtil.ItemCallback<MyPhoto>() {
        override fun areItemsTheSame(oldItem: MyPhoto, newItem: MyPhoto): Boolean {
            return oldItem === newItem
        }

        override fun areContentsTheSame(oldItem: MyPhoto, newItem: MyPhoto): Boolean {
            return oldItem.photoId == newItem.photoId
        }

    }

}

class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

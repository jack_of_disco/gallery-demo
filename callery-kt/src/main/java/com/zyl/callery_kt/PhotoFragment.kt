package com.zyl.callery_kt

import android.graphics.drawable.Drawable
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import kotlinx.android.synthetic.main.fragment_photo.*

class PhotoFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_photo, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        shimmerLayoutPhoto.apply {
            setShimmerColor(0x55FFFFFF)
            setShimmerAngle(0)
            startShimmerAnimation()
        }

        Glide.with(requireActivity())
                .load(arguments?.getParcelable<MyPhoto>("PHOTO")?.fullUrl)
                .placeholder(R.drawable.ic_photo_gray_24dp)
                .listener(object : RequestListener<Drawable> {
                    //加载失败处理
                    override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>?, isFirstResource: Boolean): Boolean {
//                        Log.d("myTAG", "onLoadFailed: "+arguments?.getParcelable<MyPhoto>("PHOTO")?.fullUrl)
//                        Log.d("myTAG", "onLoadFailed: 图片加载失败")
                        return false;
                    }
                    //加载成功处理
                    override fun onResourceReady(resource: Drawable?, model: Any?, target: Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
//                        Log.d("myTAG", "onResourceReady: 图片加载成功")
                        return false.also { shimmerLayoutPhoto?.stopShimmerAnimation() }
                    }
                })
                .into(imageView)
    }

}
package com.zyl.callery_kt

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.google.gson.Gson

class GalleryViewModel(application: Application) : AndroidViewModel(application) {
    private val _photoListLive = MutableLiveData<List<MyPhoto>>()
    val photoListLive: LiveData<List<MyPhoto>>
        get() = _photoListLive


    fun fetchData() {
        val stringRequest: StringRequest = StringRequest(
                Request.Method.GET,
                getUrl(),
                Response.Listener {
                    _photoListLive.value = Gson().fromJson(it, Pixabay::class.java).hits.toList()
                },
                Response.ErrorListener {
                    Log.d("myTAG", "fetchData: ${it.toString()}")
                }
        )
        VolleySingleton.getInstance(getApplication()).requestQueue.add(stringRequest)
    }

    private fun getUrl(): String {
        return "https://pixabay.com/api/?key=20367884-4ff0b66c3d8b80b87f646de62&q=${_keyWord.random()}"
    }

    private val _keyWord = arrayOf("car", "word", "god", "big")

}